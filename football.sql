DROP TABLE CHARACTERISTICS_PLAYERS CASCADE;
DROP TABLE AWARD CASCADE;
DROP TABLE STADIUM CASCADE;
DROP TABLE COACH CASCADE;
DROP TABLE CLUB CASCADE;
DROP TABLE CHAMPIONSHIP CASCADE;
DROP TABLE TOURNAMENT_TABLE CASCADE;
DROP TABLE PLAYER CASCADE;
DROP TABLE MATCH CASCADE;
DROP TABLE TRANSFER CASCADE;
DROP TABLE SPONSOR CASCADE;

CREATE TABLE CHARACTERISTICS_PLAYERS (
  id               BIGSERIAL PRIMARY KEY,
  power_hit        INT NOT NULL,
  accuracy_hit     INT NOT NULL,
  dribbling        INT NOT NULL,
  field_visibility INT NOT NULL,
  speed            INT NOT NULL,
  endurance        INT NOT NULL,
  reaction         INT NOT NULL,
  other            TEXT
);

CREATE TABLE STADIUM (
  id       BIGSERIAL PRIMARY KEY,
  name     VARCHAR(20) NOT NULL,
  capacity INT         NOT NULL,
  city     VARCHAR(15) NOT NULL,
  height   INT         NOT NULL
);

CREATE TABLE AWARD (
  id         BIGSERIAL PRIMARY KEY,
  name       VARCHAR(20) NOT NULL,
  decription VARCHAR(50) NOT NULL
);

CREATE TABLE COACH (
  id          BIGSERIAL PRIMARY KEY,
  experience  INT         NOT NULL,
  nationality VARCHAR(15) NOT NULL,
  award       BIGINT REFERENCES AWARD (id)
);

CREATE TABLE CLUB (
  id         BIGSERIAL PRIMARY KEY,
  name       VARCHAR(15) NOT NULL,
  coach      BIGINT      NOT NULL REFERENCES COACH (id),
  stadium    BIGINT      NOT NULL REFERENCES STADIUM (id),
  total_cost BIGINT      NOT NULL
);

CREATE TABLE TOURNAMENT_TABLE (
  id           BIGSERIAL NOT NULL UNIQUE,
  club         BIGINT    NOT NULL REFERENCES CLUB (id),
  place        INT       NOT NULL,
  points       INT       NOT NULL,
  wins         INT       NOT NULL,
  draws        INT       NOT NULL,
  losses       INT       NOT NULL,
  scored_goals INT       NOT NULL,
  conceded     INT       NOT NULL,
  difference   INT       NOT NULL
);

CREATE TABLE CHAMPIONSHIP (
  id            BIGSERIAL PRIMARY KEY,
  name          VARCHAR(15) NOT NULL,
  start_date    DATE        NOT NULL,
  end_date      DATE        NOT NULL,
  tournir_table BIGINT      NOT NULL REFERENCES TOURNAMENT_TABLE (id)
);

CREATE TABLE PLAYER (
  id              BIGSERIAL PRIMARY KEY,
  first_name      VARCHAR(20) NOT NULL,
  second_name     VARCHAR(20) NOT NULL,
  age             INT         NOT NULL,
  height          INT         NOT NULL,
  weight          INT         NOT NULL,
  nationality     VARCHAR(15) NOT NULL,
  amplua          VARCHAR(15) NOT NULL,
  club            BIGINT      NOT NULL REFERENCES CLUB (id),
  characteristics BIGINT      NOT NULL REFERENCES CHARACTERISTICS_PLAYERS (id),
  COST            INT         NOT NULL,
  award           BIGINT REFERENCES AWARD (id),
  about           TEXT
);

CREATE TABLE MATCH (
  id              BIGSERIAL PRIMARY KEY,
  championship_id BIGINT NOT NULL REFERENCES CHAMPIONSHIP (id),
  team_home       BIGINT NOT NULL REFERENCES CLUB (id),
  team_guest      BIGINT NOT NULL REFERENCES CLUB (id),
  stadium         BIGINT NOT NULL REFERENCES STADIUM (id),
  home_goals      INT    NOT NULL,
  guest_goals     INT    NOT NULL,
  replacement     INT
);

CREATE TABLE TRANSFER (
  id                 BIGSERIAL PRIMARY KEY,
  championship_id    BIGINT NOT NULL REFERENCES CHAMPIONSHIP (id),
  player             BIGINT NOT NULL REFERENCES PLAYER (id),
  from_team          BIGINT NOT NULL REFERENCES CLUB (id),
  to_team            BIGINT NOT NULL REFERENCES CLUB (id),
  transaction_amount BIGINT NOT NULL
);

CREATE TABLE SPONSOR (
  id              BIGSERIAL NOT NULL,
  championship_id BIGINT    NOT NULL REFERENCES CHAMPIONSHIP (id),
  club            BIGINT    NOT NULL REFERENCES CLUB (id),
  start_date      TIMESTAMP NOT NULL,
  end_date        TIMESTAMP NOT NULL,
  cost            INT       NOT NULL
);